import logging
import re
import pytz
from datetime import datetime
from urllib import request
from urllib.error import URLError
from collections import defaultdict


SECTIONS = {
    'latest-readings': (
        re.compile(r'^Latest readings recorded at (?P<hours>\d+):(?P<minutes>\d+) Hong Kong Time (?P<day>\d+) (?P<month>\w+) (?P<year>\d+)'),
        re.compile(r"^(?P<station>[\w\s']+?)\s+(?P<temperature>[\dN]\S+)\s+(?P<humidity>[\dN]\S+)\s+(?P<max_grass>[\dN]\S+) /\s+(?P<min_grass>[\dN]\S+)\s+")
        ),
    '10min-wind': (
        re.compile(r'^10-Minute Mean Wind Direction'),
        re.compile(r"^(?P<station>.*\S)\s+(?P<direction>\S+)\s+(?P<speed>\d+)\s+(?P<gusts>\d+)$")
        ),
    '10min-visibility': (
        re.compile(r'^10-Minute Mean Visibility'),
        re.compile(r"^(?P<station>.*\S)\s+(?P<visibility>\d+) km$")
        ),
    'pressure': (
        re.compile(r'^Mean Sea Level Pressure'),
        re.compile(r"^(?P<station>.*\S)\s+(?P<pressure>\S+)$")
        )
}

compass_direction = {'North': 0, 'Northeast': 45, 'East': 90, 'Southeast': 135,
                     'South': 180, 'Southwest': 225, 'West': 270, 'Northwest': 315}

def cd2md(direction):
    math_direction = 270 - direction
    if math_direction < 0:
        math_direction += 360
    return math_direction

def download(uri=r'http://www.hko.gov.hk/textonly/v2/forecast/text_readings_e.htm'):
    wx_txt = None
    try:
        with request.urlopen(uri) as f:
            wx_txt = f.readlines()
            logging.info('Download complete.')
    except URLError as e:
        logging.error('%s - %s' % (e, uri))
    return wx_txt


def get_section(line):
    for section, (section_pattern, line_pattern) in SECTIONS.items():
        m = section_pattern.search(line)
        if m:
            return section, m.groupdict(), m.groups(), line_pattern
    raise ValueError('No section defined here.')


def extract_data(fh):
    data = defaultdict(dict)
    report_time = None
    for line in fh:
        line = line.decode('utf-8')
        # Ignore lines that don't start with a regular character
        if not re.search(r'^\w+', line):
            continue
        try:
            (section, section_info, groups, pattern) = get_section(line)
            if section == 'latest-readings':
                logging.info('Latest readings taken at %(hours)s:%(minutes)s on %(month)s %(day)s %(year)s' % section_info)
                report_time = datetime.strptime(','.join(groups), '%H,%M,%d,%B,%Y')
                report_time = pytz.timezone('Asia/Hong_Kong').localize(report_time)
                report_time = report_time.astimezone(pytz.utc)
            continue
        except ValueError:
            pass
        m = pattern.search(line)
        if m:
            data[section][m.group('station')] = m.groupdict()
    return data, report_time



