import logging
import sqlite3
import os
from datetime import datetime, timedelta
import pandas as pd
import pandas.io.sql as psql
import pytz
from hko import wx
from jinja2 import Environment, PackageLoader
import matplotlib
import matplotlib.dates as dates
matplotlib.use('Agg')
from matplotlib import pyplot as plt
# matplotlib.style.use('ggplot')


if __name__ == '__main__':
    logging.basicConfig(filename='hko_wx.log', level=logging.DEBUG, format='%(asctime)s|%(levelname)s|%(message)s')
    logging.info('Download started.')
    txt = wx.download()
    logging.info('Download finished.')
    if txt is not None:
        logging.info('Data extraction started.')
        wx_data, report_time = wx.extract_data(txt)
        logging.info('Data extraction finished.')
        conn = sqlite3.connect('db/hko_wx.db')
        c = conn.cursor()
        c.execute('''create table if not exists wind
                     (time text, station text, direction text, speed integer, gusts integer, primary key (time, station))''')
        c.execute('''create table if not exists weblog (time text, status integer primary key)''')
        for section in wx_data:
            for station in wx_data[section]:
                entry = wx_data[section][station]
                if section == '10min-wind':
                    row = [report_time, station]
                    row.extend(entry[x] for x in 'direction speed gusts'.split())
                    logging.debug('Writing row to db: %s' % row)
                    c.execute('replace into wind values (?, ?, ?, ?, ?)', row)
        conn.commit()
        c.execute('''select time from weblog where status=?''', [1])
        try:
            last_built = datetime.strptime(''.join(c.fetchone()[0].rsplit(':', 1)), '%Y-%m-%d %H:%M:%S%z')
        except TypeError:
            last_built = datetime.now() + timedelta(days=-1)
            last_built = pytz.timezone('Asia/Hong_Kong').localize(last_built)
            last_built = last_built.astimezone(pytz.utc)
        if last_built < report_time:
        # if True:
            pth = 'www/wxplots'
            if not os.path.exists(pth):
                os.makedirs(pth)
            logging.info('Making plots started.')
            # make json
            df = psql.read_sql("select * from wind where datetime(time) > datetime('now', '-24 hours')", conn,
                               index_col='time', parse_dates={'time': '%Y-%m-%d %H:%M:%S'})
            # times = pd.Index(df['times'])
            # times2 = times.tz_localize(pytz.utc)
            # times2 = times2.tz_convert('Asia/Hong_Kong')
            # df.set_index(times2, inplace=True)
            # df.drop('time', axis=1, inplace=True)
            # print(df)
            units = {'km/h': 1.0, 'kts': 1.0/1.852}
            for station in ['Waglan Island', "Tate's Cairn",
                            'Ngong Ping', 'Sai Kung', 'Cheung Chau', 'Cheung Chau Beach']:
                for unit in units:
                    gusts = df[df.station == station].gusts * units[unit]
                    speed = df[df.station == station].speed * units[unit]
                    ewma = speed.ewm(min_periods=0, ignore_na=False, adjust=True, span=3).mean()
                    if speed.empty:
                        continue
                    f = plt.figure()
                    f.set_size_inches(11, 5)
                    ax = f.add_subplot(111)
                    plt.plot(speed, label='%s / average' % station)
                    plt.plot(gusts, label='%s / gusts' % station)
                    plt.plot((gusts-speed), '--', label='%s / diff' % station)
                    plt.tick_params(labelright=True, labelleft=True)
                    plt.ylim(0, gusts.max() * 1.15)
                    plt.ylabel(unit)
                    plt.legend(loc=2, prop={'size': 8})
                    ax.xaxis.set_major_locator(dates.HourLocator())
                    ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M', pytz.timezone('Asia/Hong_Kong')))
                    plt.xticks(rotation=70)
                    ax.xaxis.grid(True, which="major")
                    ax.yaxis.grid(True, which="major")
                    # xticks = speed.resample('H').count().index
                    plt.title('%s to %s' % (speed.index.min().tz_localize('utc').tz_convert('Asia/Hong_Kong'),
                                            speed.index.max().tz_localize('utc').tz_convert('Asia/Hong_Kong')))
                    # picpath = '%s.png' % re.sub('\W', '', station)
                    picpath = os.path.join(pth, '%s_%s.png' % (station.replace(' ', '').replace("'", ''), unit.split('/')[0]))
                    plt.savefig(picpath, bbox_inches='tight')
                    plt.gcf().clear()
            c.execute('''replace into weblog values(?, ?)''', [report_time, 1])
            conn.commit()
            logging.info('Making plots finished.')

            env = Environment(loader=PackageLoader('hko', 'templates'))
            template = env.get_template('main.html')
            # print(template.render(report_time=report_time))
            # print(df.loc[report_time:].to_dict())
            # print(df.loc[report_time])
            cdata = [r for r in df.loc[report_time].to_dict('records')
                     if r['station'] in ['Waglan Island', "Tate's Cairn",
                                         'Ngong Ping', 'Sai Kung', 'Cheung Chau', 'Cheung Chau Beach']]
            # print(cdata)
            template.stream(report_time=report_time, data=sorted(cdata, key=lambda d: d['station'])).dump('www/index.html')
            # Template('Hello {{ name }}!').stream(name='foo').dump('hello.html')
    else:
        logging.error('No wx data returned from the website.')
    logging.info('All done.')

